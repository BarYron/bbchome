import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Users } from './interface/users';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

apiUrl='https://jsonplaceholder.typicode.com/users/';

  constructor(private http: HttpClient) { }
  getUsers(){
  return this.http.get<Users[]>(this.apiUrl)
  }
}