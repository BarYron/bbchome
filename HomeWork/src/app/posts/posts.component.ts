import { PostsService } from '../posts.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Post } from '../inteface/post';


import { Users } from '../interface/users';

import {Observable} from 'rxjs'
import { throwMatDialogContentAlreadyAttachedError } from '@angular/material';
import { UsersService } from '../users.service';
import { AuthService } from '../auth.service';


@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent  implements OnInit {

  panelOpenState = false;

  postData$:Observable<any>;
 // users$: Users[]=[];
  title:string;
  body:string;
  author:string;
  ans:string;
  userId:string;

constructor(private postservice:PostsService,
  public authService:AuthService) { }
  
//deletePost(id:string){
  //this.postsService.deletePost(id);
//}

deletePost(id){
  this.postservice.deletePost(this.userId,id)
  console.log(id);
}


/* myFunc(){
  for (let index = 0; index < this.postData$.length; index++) {
    for (let i = 0; i < this.users$.length; i++) {
      if (this.postData$[index].userId==this.users$[i].id) {
        this.title = this.postData$[index].title;
        this.body = this.postData$[index].body;
        this.author = this.users$[i].name;
        this.postsService.addPosts(this.body,this.author,this.title);
        
      }
      
      
    }
    
  }
  this.ans ="The data retention was successful"
}*/

 

  onSubmit() {
  //  this.usersService.getUsers().subscribe(data => this.users$ = data)
  //  this.postsService.getPosts().subscribe(data => this.postData$ = data)
  }

  ngOnInit() {
    //geting posts from db
    
       // this.postData$ = this.postservice.getPosts();
        
    //geting posts from json
         //this.usersService.getUsers() .subscribe(data => this.users$ = data)
        //this.postsService.getPosts() .subscribe(data => this.posts$ = data)
       
      
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;
        this.postData$ = this.postservice.getPosts(this.userId); 
      }
    )
      }
}