import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { BooksComponent } from './books/books.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavComponent } from './nav/nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';

import {MatExpansionModule} from '@angular/material/expansion';
import {MatCardModule} from '@angular/material/card';
import { AuthorsComponent } from './authors/authors.component';
import { RouterModule, Routes } from '@angular/router';
import { EditauthorComponent } from './editauthor/editauthor.component';
import { FormsModule }   from '@angular/forms';
import {MatFormFieldModule} from '@angular/material/form-field';
import { PostsComponent } from './posts/posts.component';
import {MatInputModule} from '@angular/material';
import {CdkTableModule} from '@angular/cdk/table';

import { HttpClientModule } from '@angular/common/http';
import { PostsService } from './posts.service';
///////////////

import {MatSelectModule} from '@angular/material';

// Firebase
import { AngularFireModule } from '@angular/fire';
import { environment } from '../environments/environment';
// Firebase modules
import { AngularFirestoreModule, AngularFirestore } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { PostformComponent } from './postform/postform.component';



import { AngularFireAuth } from '@angular/fire/auth';
import { SignUpComponent } from './sign-up/sign-up.component';
import { LoginComponent } from './login/login.component';

//////REWR/////T/////////REWR///////////H/



















const appRoutes: Routes = [
  { path: 'books', component: BooksComponent },
  { path: 'authors/:id/:name', component: AuthorsComponent },
  { path: 'editauthor/:id', component: EditauthorComponent},
  { path: 'postform', component: PostformComponent},
  { path: 'postform/:id', component: PostformComponent},
  { path: 'posts', component:PostsComponent },
  { path: 'signup', component: SignUpComponent},
  { path: 'login', component: LoginComponent},
  { path: '',
    redirectTo: '/books',
    pathMatch: 'full'
  },
];

@NgModule({
  declarations: [
    AppComponent,
    BooksComponent,
    NavComponent,
    AuthorsComponent,
    EditauthorComponent,
    PostsComponent,
    PostformComponent,
    SignUpComponent,
    LoginComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatInputModule,
    HttpClientModule,
    CdkTableModule,
    MatListModule,
    MatCardModule,
    MatExpansionModule,
    FormsModule,
    MatFormFieldModule,
    AngularFireModule.initializeApp(environment.firebaseConfig, 'homeproject-cf724'),
    AngularFireStorageModule,
    AngularFireAuthModule,

    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    )    
  ],
  providers: [AngularFireAuth,AngularFirestore],
  bootstrap: [AppComponent]
})
export class AppModule { }