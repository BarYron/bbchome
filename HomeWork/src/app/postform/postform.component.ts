import { Component, OnInit } from '@angular/core';
import { PostsService } from '../posts.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-postform',
  templateUrl: './postform.component.html',
  styleUrls: ['./postform.component.css']
})
export class PostformComponent implements OnInit {

  constructor(private postsservice:PostsService, private router:Router,
    private authService:AuthService, private route:ActivatedRoute) { }
  
 body:string;
  title:string;
  id:string;
  author:string;
  isEdit:boolean = false;
  userId:string;
buttonText:string = "Add Post";
  onSubmit(){ 
if(this.isEdit){
this.postsservice.updatePost(this.userId, this.id, this.title, this.body,this.author)
}else{

    this.postsservice.addPost(this.userId,this.title,this.author,this.body)
}
    this.router.navigate(['/posts']);
  }  
 

  ngOnInit() {
this.id = this.route.snapshot.params.id;
this.authService.getUser().subscribe(
  user => {
this.userId = user.uid; 
if(this.id){
this.isEdit = true;
this.buttonText = "Update post";
this.postsservice.getPost(this.userId,this.id).subscribe(
  post => {
    console.log(post.data().author)
    console.log(post.data().title)
    this.author = post.data().author;
    this.title = post.data().title;
    this.body = post.data().body;})
  }
})
}
  

}
