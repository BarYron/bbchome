export interface Users {
    id:number,
    name:string ,
    username: string,
    email: string,
    street: string,
    suite:string,
    city: string,
    zipcode: string,
    lat: number,
    lng: number
    phone: string,
    website: string
}
