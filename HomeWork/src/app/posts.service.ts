
import { Injectable } from '@angular/core';
import { HttpClientModule,HttpClient } from '@angular/common/http';
import { Post } from './inteface/post';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { AuthorsComponent } from './authors/authors.component';
import { AuthService } from './auth.service';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class PostsService  {
   URL='https://jsonplaceholder.typicode.com/posts/';
  
   userCollection:AngularFirestoreCollection = this.db.collection('users');
   postCollection:AngularFirestoreCollection
   
  // constructor(private _http:HttpClient,private db:AngularFirestore) { }
 
  constructor(private db: AngularFirestore,
    private authService:AuthService) {}
    
   addPosts(body:string, author:string,title:string){
   const post = {body:body, author:author, title:title};
    this.db.collection('posts').add(post);
  }

  addPost(userId:string, title:string, author:string , body:string){
     
    const post = {body:body, author:author, title:title};
    //this.db.collection('books').add(book)  
    this.userCollection.doc(userId).collection('posts').add(post);
  } 


   //deletePost(id:string){
    //this.db.doc(`posts/${id}`).delete();
  //}

  deletePost(userId:string, id:string){
    this.db.doc(`users/${userId}/posts/${id}`).delete();
  }

  updatePost(userId:string, id:string,title:string,author:string, body:string){
    this.db.doc(`users/${userId}/posts/${id}`).update(
      {
        title:title,
        author:author,
        body:body,
      }
    )
  }

  //updatePost(id:string, title:string, body:string, author:string){
    //this.db.doc(`posts/${id}`).update({
    //title:title,
    //body:body,
    //author:author,
    //})
  //}




  //addPost(title:String,  author:String, body:String){
  //  const post = {title:title, author:author, body:body}
  //  this.db.collection('posts').add(post)  
 // }  
  
  //getPost(id:string):Observable<any>{
  // return this.db.doc(`posts/${id}`).get();
 // }

 getPost(userId:string, id:string):Observable<any>{
  return this.db.doc(`users/${userId}/posts/${id}`).get();
}

 // getPosts(){
//    return this._http.get<Post[]>(this.URL)
// }

 // getPosts():Observable<any[]>{
 // return this.db.collection('posts').valueChanges({idField:'id'});
 // }

 getPosts(userId): Observable<any[]> {
  //const ref = this.db.collection('books');
  //return ref.valueChanges({idField: 'id'});
  this.postCollection = this.db.collection(`users/${userId}/posts`);
  console.log('post collection created');
  return this.postCollection.snapshotChanges().pipe(
    map(collection => collection.map(document => {
      const data = document.payload.doc.data();
      data.id = document.payload.doc.id;
      return data;
    }))
  );    
} 


}
